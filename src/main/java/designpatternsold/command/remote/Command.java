package designpatternsold.command.remote;

public interface Command {
	public void execute();
}
