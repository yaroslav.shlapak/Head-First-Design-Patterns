package designpatternsold.combined.djview;
  
public interface BeatObserver {
	void updateBeat();
}
