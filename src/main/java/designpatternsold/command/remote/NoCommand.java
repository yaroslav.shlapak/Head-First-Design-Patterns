package designpatternsold.command.remote;

public class NoCommand implements Command {
	public void execute() { }
}
