package designpatternsold.strategy;

public interface QuackBehavior {
	public void quack();
}
