package designpatternsold.combining.decorator;

public interface Quackable {
	public void quack();
}
