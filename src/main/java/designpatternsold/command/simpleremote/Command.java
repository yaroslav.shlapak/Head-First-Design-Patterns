package designpatternsold.command.simpleremote;

public interface Command {
	public void execute();
}
