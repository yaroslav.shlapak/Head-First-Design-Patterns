package designpatternsold.observer.weatherobservable;

public interface DisplayElement {
	public void display();
}
