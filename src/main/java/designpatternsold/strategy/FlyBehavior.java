package designpatternsold.strategy;

public interface FlyBehavior {
	public void fly();
}
