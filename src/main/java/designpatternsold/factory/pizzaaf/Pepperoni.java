package designpatternsold.factory.pizzaaf;

public interface Pepperoni {
	public String toString();
}
