package designpatternsold.factory.pizzaaf;

public interface Sauce {
	public String toString();
}
