package designpatternsold.observer.weather;

public interface DisplayElement {
	public void display();
}
