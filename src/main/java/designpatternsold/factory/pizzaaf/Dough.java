package designpatternsold.factory.pizzaaf;

public interface Dough {
	public String toString();
}
