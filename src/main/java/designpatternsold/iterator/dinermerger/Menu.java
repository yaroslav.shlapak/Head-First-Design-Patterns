package designpatternsold.iterator.dinermerger;

public interface Menu {
	public Iterator createIterator();
}
