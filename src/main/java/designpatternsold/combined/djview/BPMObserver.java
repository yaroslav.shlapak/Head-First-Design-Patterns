package designpatternsold.combined.djview;
  
public interface BPMObserver {
	void updateBPM();
}
