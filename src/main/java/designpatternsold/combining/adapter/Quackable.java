package designpatternsold.combining.adapter;

public interface Quackable {
	public void quack();
}
