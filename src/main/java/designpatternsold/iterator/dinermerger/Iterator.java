package designpatternsold.iterator.dinermerger;

public interface Iterator {
	boolean hasNext();
	Object next();
}
