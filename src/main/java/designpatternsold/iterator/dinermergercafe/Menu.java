package designpatternsold.iterator.dinermergercafe;

import java.util.Iterator;

public interface Menu {
	public Iterator createIterator();
}
