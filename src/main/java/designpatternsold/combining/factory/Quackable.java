package designpatternsold.combining.factory;

public interface Quackable {
	public void quack();
}
