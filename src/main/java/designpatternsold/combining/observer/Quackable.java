package designpatternsold.combining.observer;

public interface Quackable extends QuackObservable {
	public void quack();
}
